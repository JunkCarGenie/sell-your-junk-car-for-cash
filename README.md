What do you do with an old useless automobile? You junk it of course and with the Genie. We make the process easy and convenient. 
Simply call or email us and we'll send you an offer letter, once you agree, we'll set up a day to pick up your car free of charge. 
At Junk Car Genie we pay you for recycling your junk vehicle! Help the environment and make some money in the process by helping reduce waste in your cities landfill.
We take your junk vehicle and recycle it for scrap metal. The Genie is ready to give you cash for your junk car.		
https://www.junkcargenie.com/
